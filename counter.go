package counter

import "sync"

type Data map[string]uint32
type Counter struct {
	data Data
	inc chan string
	do chan func(Data)
	wg sync.WaitGroup
}

func (c *Counter) worker() {
	for {
		select {
		case key := <-c.inc:
			c.data[key]++
		case f := <-c.do:
			f(c.data)
		}
		c.wg.Done()
	}
}

func (c *Counter) Inc(key string) {
	c.wg.Add(1)
	c.inc <- key
}

func (c *Counter) Do(f func(Data)) {
	c.wg.Add(1)
	c.do <- f
}

func (c *Counter) Wait() {
	c.wg.Wait()
}

func New() *Counter {
	c := Counter {
		data: make(Data),
		inc: make(chan string, 100),
		do: make(chan func(Data), 100),
	}
	go c.worker()
	return &c
}
